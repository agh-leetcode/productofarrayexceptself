import java.util.*;

class Solution {

    public static void main(String[]args){
        System.out.println(Arrays.toString(new Solution().productExceptSelf(new int[]{1,2,3,4,5}));
    }

    private int[] productExceptSelf(int[] nums) {
        int [] result = new int[nums.length];
        int before = 1;
        int after = 1;
        for(int i=0; i<nums.length;i++){
            result[i] = before;
            before *= nums[i];
        }
        for(int j=nums.length-1; j >= 0; j--){
            result[j] *= after;
            after *= nums[j];
        }

        return result;
    }
}